class TopMenu < SitePrism::Section
  element :sign_in_link, '.login'
  element :sign_out_link, '.logout'
  element :projects_link, '.projects'
end