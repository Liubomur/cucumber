class ConfDelProj < SitePrism::Page

  element :confirm_checkbox, '#confirm'
  element :delete_button, '[name="commit"]'



  section :top_menu, TopMenu, '#top-menu'
end