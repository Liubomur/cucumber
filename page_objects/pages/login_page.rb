class LoginPage < SitePrism::Page
  set_url'http://127.0.0.1/redmine/login'

  element :user_name_field, '#username'
  element :password_field, '#password'
  element :submit_button, '#login-submit'
end