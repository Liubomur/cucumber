class RegisterPage < SitePrism::Page
  set_url 'http://127.0.0.1/redmine/account/register'

  section :top_menu, TopMenu, '#top-menu'

  element :Login_field, '#user_login'
  element :Password_field, '#user_password'
  element :Confirmation_field, '#user_password_confirmation'
  element :First_name_field, '#user_firstname'
  element :Last_name_field, '#user_lastname'
  element :Email_field, '#user_mail'

  element :Submit_btn, :xpath, '//*[@id="new_user"]/input[3]'
end