class NewProjectPage < SitePrism::Page
  set_url'http://127.0.0.1/redmine/projects/new'
  element :new_project_name_field, '#project_name'
  element :new_project_desc_field, '#project_description'
  element :new_project_identifier_field, '#project_identifier'
  element :new_project_homepage_field, '#project_homepage'
  element :new_project_publicity_checkbox, '#project_is_public'
  element :new_project_forums_checkbox, '#project_enabled_module_names_boards'
  element :new_project_create_button, :xpath,'//*[@id="new_project"]/input[3]'

  section :top_menu, TopMenu, '#top-menu'
end

