class ProjectsPage < SitePrism::Page
  set_url'http://127.0.0.1/redmine/projects'
  element :new_project_link, '.icon-add'
  element :administration_link, '.icon-settings'

  section :top_menu, TopMenu, '#top-menu'
end

