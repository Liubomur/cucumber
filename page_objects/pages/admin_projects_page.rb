class AdmProjectsPage < SitePrism::Page
set_url'admin/projects'
element :project_search_field, '#name'
element :apply_button, :xpath, '//*[@id="content"]/form/fieldset/input[2]'
element :project_delete_link, '[data-method="delete"]'


section :top_menu, TopMenu, '#top-menu'
end