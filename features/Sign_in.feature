Feature: Sign in
  As a visitor
  I want tobe able to sign in
  In order to become a user


  Scenario: Sign in positive flow
    Given I am not logged in visitor (on page)

     When I click the Sign in button
     Then Log in form is displayed

     When I fill in Log in form with valid credentials
      And I click login button
     Then I become a logged in user
