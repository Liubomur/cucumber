Given(/^I am not registered visitor \(on 'account register' page\)$/) do
  @register_page = RegisterPage.new
  @register_page.load

end

When(/^I fill required fields$/) do
  @new_id = Time.now.to_i.to_s
  @register_page.Login_field.set 'test_user' + @new_id
  @register_page.Password_field.set '12345678'
  @register_page.Confirmation_field.set '12345678'
  @register_page.First_name_field.set 'firstname_test_user' + @new_id
  @register_page.Last_name_field.set 'lastname_test_user' + @new_id
  @register_page.Email_field.set 'test_user' + @new_id + '@test.com'
  sleep 2
end

And(/^I click 'Submit' button$/) do
  @register_page.Submit_btn.click
  sleep 2
end


Then(/^I become a logged in user on 'my account' page$/) do
  expect(current_url).to include '/redmine/my/account'
  expect(page).to have_content 'Logged in as ' + 'test_user'
  expect(page).to have_content 'Your account has been activated. You can now log in'
end

And(/^Sign out$/) do
  @register_page.top_menu.sign_out_link.click
end

And(/^Sign in$/) do

  login 'test_user' + @new_id, 12345678
  sleep 2
end