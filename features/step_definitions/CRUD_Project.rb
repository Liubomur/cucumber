#Create

When(/^I click 'Projects' button$/) do
  @home_page = HomePage.new
  @home_page.top_menu.projects_link.click

end

Then(/^'Projects' page is displayed$/) do
  expect(current_url).to include '/projects'
  expect(page).to have_content 'New project'
end

When(/^I click 'New project' button$/) do
  @projects_page = ProjectsPage.new
  @projects_page.new_project_link.click
end

Then(/^'New Project' page is displayed$/) do
  expect(current_url).to include '/projects/new'
  expect(page).to have_content 'Length between 1 and 100 characters. Only lower case letters (a-z), numbers, dashes and underscores are allowed.
Once saved, the identifier cannot be changed.'
end

@@proj_name = Time.now.to_i.to_s

When(/^I fill in required fields$/) do

  @new_project_page = NewProjectPage.new
  @new_project_page.new_project_name_field.set 'test project ' + @@proj_name
  @new_project_page.new_project_desc_field.set 'test project description ' + @@proj_name
  @new_project_page.new_project_homepage_field.set 'test project homepage ' + @@proj_name
  @new_project_page.new_project_publicity_checkbox.uncheck
  @new_project_page.new_project_forums_checkbox.uncheck

  sleep 1
end

And(/^I click 'Create' button$/) do
  @new_project_page.new_project_create_button.click
  sleep 1
end

Then(/^New project is created$/) do
  expect(current_url).to include @@proj_name
  expect(page).to have_content 'Successful creation.'

end

#Update

When(/^I click 'Administration' button$/) do
  @projects_page = ProjectsPage.new
  @projects_page.administration_link.click
end

Then(/^'Admin\-Projects' page is displayed$/) do
  expect(current_url).to include 'admin/projects'
  expect(page).to have_content 'Administration'
end


When(/^I find newly created Projects and click on it's name$/) do
  @admin_projects_page = AdmProjectsPage.new
  @admin_projects_page.project_search_field.set 'test project ' + @@proj_name
  sleep 1
  @admin_projects_page.apply_button.click
  @admin_projects_page.find("a", :text => 'test project ' + @@proj_name).click
  sleep 1
end

Then(/^'settings' page is displayed$/) do
  expect(current_url).to include 'test-project-' + @@proj_name
  expect(page).to have_content 'test project ' + @@proj_name
end

When(/^I edit all fields$/) do
  @proj_set_page = ProjectSettingsPage.new
  @proj_set_page.new_project_name_field.set 'test project ' + @@proj_name + 'a'
  @proj_set_page.new_project_desc_field.set 'test project description ' + @@proj_name + 'a'
  @proj_set_page.new_project_homepage_field.set 'test project homepage ' + @@proj_name + 'a'
  @proj_set_page.new_project_publicity_checkbox.check
  @proj_set_page.new_project_forums_checkbox.check
  @proj_set_page.new_project_save_button.click
  sleep 1

end

And(/^I click 'Save' button$/) do
  @proj_set_page.new_project_save_button.click
  sleep 1
end

Then(/^Project is updated$/) do
  expect(current_url).to include 'test-project-' + @@proj_name
  expect(page).to have_content 'test project ' + @@proj_name + 'a'
  expect(page).to have_content 'Successful update.'
end

#Delete

When(/^I find newly created Projects and click 'Delete'$/) do
  @admin_projects_page = AdmProjectsPage.new
  @admin_projects_page.project_search_field.set 'test project ' + @@proj_name + 'a'
  sleep 1
  @admin_projects_page.apply_button.click
  @admin_projects_page.project_delete_link.click
  sleep 1
end

Then(/^'Confirm delete' page is displayed$/) do
  expect(page).to have_content 'test project ' + @@proj_name + 'a'
  expect(page).to have_content 'Confirmation'
end

When(/^I colfirm deletion$/) do
  @conf_del_proj_page = ConfDelProj.new
  @conf_del_proj_page.confirm_checkbox.click
end

And(/^I click 'Delete' button$/) do
  @conf_del_proj_page.delete_button.click
  sleep 1
end

Then(/^Project is deleted$/) do
  @admin_projects_page.project_search_field.set 'test project ' + @@proj_name + 'a'
  @admin_projects_page.apply_button.click
  expect(page).to have_content 'No data to display'
  sleep 5
end