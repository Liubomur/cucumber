Given(/^I am not logged in visitor \(on page\)$/) do
  #visit 'http://127.0.0.1/redmine/'
  @home_page = HomePage.new
  @home_page.load
end

When(/^I click the Sign in button$/) do
  #find('.login').click
   @home_page.top_menu.sign_in_link.click
end

Then(/^Log in form is displayed$/) do
  expect(current_url).to include '/login'
  expect(page).to have_content 'Login'
  expect(page).to have_content 'Password'
end

When(/^I fill in Log in form with valid credentials$/) do
  @login_page = LoginPage.new
  #find('#username').set 'user'
  #find('#password').set 'bd26061980'
  @login_page.user_name_field.set 'user'
  @login_page.password_field.set 'bd26061980'
end

And(/^I click login button$/) do
  #find('#login-submit').click
  @login_page.submit_button.click
end

Then(/^I become a logged in user$/) do
  expect(page).to have_content 'Logged in as user'
end




Given(/^I am logged in as "([^"]*)"$/) do |user_name|
  login user_name, '12345678'
end

Then(/^I become a logged in "([^"]*)"$/) do |user_name|
   expect(page).to have_content 'Logged in as ' + user_name
end


Given(/^I am logged in as "([^"]*)" with password "([^"]*)"$/) do |user_name, pass|
    login user_name, pass
 end