Feature: Sign in
  As a visitor
  I want tobe able to sign in
  In order to become a user


  Scenario: Sign in as user, positive flow
    Given I am logged in as "test9210743565" with password "12345678"
     Then I become a logged in "test"


  Scenario: Sign in as admin, positive flow
    Given I am logged in as "user" with password "bd26061980"
    Then I become a logged in "user"