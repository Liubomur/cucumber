Feature: Register user
  As a visitor
  I want tobe able to register
  In order to create an account

 Scenario: Register user positive flow
  Given I am not registered visitor (on 'account register' page)

  When I fill required fields
   And I click 'Submit' button
  Then I become a logged in user on 'my account' page
   And Sign out
   And Sign in