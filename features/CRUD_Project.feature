Feature: Create, update, delete project
   As a user I want to be able
   to create new project
   to update existing project
   to delete existing project

  Scenario: Create project
    Given I am logged in as "user" with password "bd26061980"
    When I click 'Projects' button
    Then 'Projects' page is displayed
    When I click 'New project' button
    Then 'New Project' page is displayed
    When I fill in required fields
     And I click 'Create' button
    Then New project is created

  Scenario: Update project
    Given I am logged in as "user" with password "bd26061980"
    When I click 'Projects' button
    Then 'Projects' page is displayed
    When I click 'Administration' button
    Then 'Admin-Projects' page is displayed
    When I find newly created Projects and click on it's name
    Then 'settings' page is displayed
    When I edit all fields
     And I click 'Save' button
    Then Project is updated

  Scenario: Update project
    Given I am logged in as "user" with password "bd26061980"
    When I click 'Projects' button
    Then 'Projects' page is displayed
    When I click 'Administration' button
    Then 'Admin-Projects' page is displayed
    When I find newly created Projects and click 'Delete'
    Then 'Confirm delete' page is displayed
    When I colfirm deletion
    And I click 'Delete' button
    Then Project is deleted