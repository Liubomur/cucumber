require 'rest-client'

module APIHelper
  def get_user_list
    RestClient.get 'http://127.0.0.1/redmine/users.json', {'Content-Type' => 'application/json', 'X-Redmine-API-Key' => '35cb069e05e6eaa2988968c01e979e0fe6ae2f69'}
  end

  def register_user_api
    new_id = Time.now.to_i.to_s
    payload = {
        "user" => {
            "login" => "test" + new_id,
            "firstname" => "Jean-Philippe",
            "lastname"  => "Lang",
            "mail"  => "jp_lang" + new_id + "@yahoo.fr",
            "password" => "123456789"
        }
    }.to_json
    RestClient.post 'http://127.0.0.1/redmine/users.json', payload, {'Content-Type' => 'application/json', 'X-Redmine-API-Key' => '35cb069e05e6eaa2988968c01e979e0fe6ae2f69'}
  end
end
