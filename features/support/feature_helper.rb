module FeatureHelper

  def login(user_name, password)
    @login_page = LoginPage.new
    @login_page.load

    @login_page.user_name_field.set user_name
    @login_page.password_field.set password
    @login_page.submit_button.click
  end


end